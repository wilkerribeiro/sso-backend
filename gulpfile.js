const gulp = require("gulp");
const ts = require("gulp-typescript");
const tslint = require("gulp-tslint");
const clean = require("gulp-clean");
const watch = require('gulp-watch');
const nodemon = require('gulp-nodemon')
const sourcemaps = require('gulp-sourcemaps');

const tsProject = ts.createProject("tsconfig.json");

gulp.task("lint", function () {
	gulp.src("src/**/*.ts")
		.pipe(tslint({}))
		.pipe(tslint.report({
			emitError: true,
			summarizeFailureOutput: false
		}));
});
gulp.task("clean", function () {
	return gulp.src('dist/*', {
		read: false
	})
		.pipe(clean())
});
gulp.task("build:typescript", ['clean'], function () {
	return tsProject.src()
		.pipe(sourcemaps.init())
		.pipe(tsProject()).js
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest("dist"));
});
gulp.task("watch:typescript", function () {
	return gulp.watch('src/**/*.ts', ['build:typescript']);
});

gulp.task('serve', ['build:typescript', 'watch:typescript'], function () {
	return nodemon({
		script: 'dist/main.js',
		ext: 'js',
		env: { 'NODE_ENV': 'development' }
	})
})
