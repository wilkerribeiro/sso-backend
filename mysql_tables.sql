create table perfil
(
	id int PRIMARY KEY AUTO_INCREMENT,
	codigo varchar(40) not null UNIQUE,
	descricao varchar(100)
);

create table usuario
(
	id int PRIMARY KEY AUTO_INCREMENT,
	login varchar(60) not null UNIQUE,
	hash varchar(60) not null UNIQUE
);


create table pessoa
(
	id int PRIMARY KEY AUTO_INCREMENT,
	usuario_id int not null UNIQUE,
	primeiro_nome varchar(30),
	segundo_nome varchar(30),
	email varchar(60),
	cpf varchar(11),
	CONSTRAINT FKPessoaUsuario FOREIGN KEY (usuario_id) REFERENCES usuario(id)
);


create table sistema
(
	id int PRIMARY KEY AUTO_INCREMENT,
	codigo varchar(40) not null UNIQUE,
	nome varchar(60) not null UNIQUE,
	endereco varchar(100) not null UNIQUE
);

create table permissao
(
	id int PRIMARY KEY AUTO_INCREMENT,
	perfil_id int not null,
	codigo varchar(40) not null UNIQUE,
	descricao varchar(100),
	CONSTRAINT FKPermisssaoPerfil FOREIGN KEY (perfil_id) REFERENCES perfil(id)
);


create table perfil_usuario
(
	usuario_id int,
	perfil_id int,
	CONSTRAINT PKPerfilUsuarioComposta PRIMARY KEY (usuario_id, perfil_id),
	CONSTRAINT FKPerfilUsuarioUsuario FOREIGN KEY (usuario_id) REFERENCES usuario(id),
	CONSTRAINT FKPerfilUsuarioPerfil FOREIGN KEY (perfil_id) REFERENCES perfil(id)
);


drop table perfil;
drop table usuario;
drop table pessoa;
drop table sistema;
drop table permissao;
drop table perfil_usuario;

insert into sistema values('sistema-um', 'Sistema Um', 'localhost:4201')