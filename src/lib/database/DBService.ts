import { format, IError, IConnection, createPool } from "mysql";
const pool = createPool({
	connectionLimit: 100, // important
	host: "localhost",
	user: "root",
	password: "123456",
	database: "sso_backend",
	debug: false
	/*ssl  : {
		ca : readFileSync('./sslcert/rds-ssl-ca-cert.pem')
	}*/
});

export default class DBService {
	constructor() { }
	static quit() {
		pool.end();
	}

	static getConnection(cb: (err: IError, connection: IConnection) => void) {
		pool.getConnection(function (err: IError, connection: IConnection) {
			if (err) {
				console.log(err);
				// connection.release();
				cb(err, connection);
				//
				return;
			}
			// console.log('connected as id ' + connection.threadId);
			cb(err, connection);

			connection.on("error", function (err) {
				cb(err, connection);
				console.log("Error in na query");
				// res.json({"code" : 100, "status" : "Error in connection database"});
				return;
			});
		});
	}



	static executarQuery(instrucao: string, argumentos: any[], cb: (err: IError, rows: any) => any) {
		console.log(format(instrucao, argumentos));
		this.getConnection(function (err: IError, connection: IConnection) {
			if (err) {
				cb(err, undefined);
				return;
			}
			connection.query(instrucao, argumentos, function (err, rows) {
				connection.release();
				if (!err) {
					cb(err, rows);
				}
				else {
					cb(err, rows);
					console.log(err);
				}
			});
		});
	}
}