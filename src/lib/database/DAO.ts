// const express = require('express');
"use strict";
import SQLStringBuilder from "./SqlStringBuilder";
import { readFileSync } from "fs";
import DBService from "./DBService";
import { IError } from "mysql";
const mysql = require("mysql");
function processarWheres(sqlString: string, where: any) {
	let first = true;
	const whereKeys = [];
	for (const key in where) {
		if (where.hasOwnProperty(key)) {
			if (!first) {
				sqlString += " AND ?";
			}
			first = false;
			const whereKey: any = {};
			whereKey[key] = where[key];
			whereKeys.push(whereKey);
		}
	}
	return {where: whereKeys, sqlString};
}

export default class DAO extends DBService {
	static tableName: string;
	static idColuna: string = "id";

	static count(cb: (err: IError, data: any) => any) {
		this.executarQuery(`SELECT count(*) FROM ??`, [this.tableName], function (err, data) {
			cb(err, data);
		});
	}

	static findOne(where: any, cb: (err: IError, data: any) => any) {
		const sqlString = `SELECT * FROM ?? WHERE ?`;
		const options = processarWheres(sqlString, where);
		this.executarQuery(options.sqlString, [this.tableName, ...options.where], function (err, data) {
			if (data && data.length) {
				data = data[0];
			}
			cb(err, data);
		});
	}

	static getAll(cb: (err: IError, data: any[]) => any) {
		this.executarQuery(`SELECT * FROM ??`, [this.tableName], function (err, data) {
			cb(err, data);
		});
	}

	static getAllWhere(where: any, cb: (err: IError, data: any[]) => any) {
		const sqlString = `SELECT * FROM ?? WHERE ?`;
		const options = processarWheres(sqlString, where);
		this.executarQuery(options.sqlString, [this.tableName, ...options.where], function (err, data) {
			cb(err, data);
		});
	}

	static insert(obj: any, cb: (err: IError, response: any) => any) {
		this.executarQuery(`INSERT INTO ?? SET  ?`, [this.tableName, obj], function (err, response) {
			cb(err, response);
		});
	}

	static update(obj: any, cb: (err: IError, response: any) => any) {
		const id = obj[this.idColuna];
		delete obj[this.idColuna];
		this.executarQuery(`UPDATE ?? SET ? WHERE ${this.idColuna} = ?`, [this.tableName, obj, id], function (err, response) {
			cb(err, response);
		});
	}

	// UPSERT
	static save(obj: any, cb: (err: IError, response: any) => any) {
		const objCompleto = JSON.parse(JSON.stringify(obj));
		delete obj[this.idColuna];
		this.executarQuery(`INSERT INTO ?? SET ? ON DUPLICATE KEY UPDATE ?`, [this.tableName, objCompleto, obj], function (err, response) {
			cb(err, response);
		});
	}

	static deleteById(id: number, cb: (err: IError, response: any) => any) {
		this.executarQuery(`DELETE FROM ?? WHERE ${this.idColuna} = ?`, [this.tableName, id], function (err, response) {
			cb(err, response);
		});
	}

	static deleteWhere(where: any, cb: (err: IError, response: any) => any) {
		this.executarQuery(`DELETE FROM ?? WHERE ?`, [this.tableName, where], function (err, response) {
			cb(err, response);
		});
	}
}
// console.log(sqlString)

/*dao.deleteById('user', 4, function(err, result){
	console.log(result)
	dao.getAll('user', function(err,users){
			console.log(users)
		})
})*/
// dao.update({id:9, login:'testeupdate3', password: 'testeupdate2'}, function(err, user){
// 	dao.getAll(function(err,users){
// 		console.log(users)
// 	})
// })


/*dao.executarQuery(sqlString.string , sqlString.argumentos, function(err, data){
	console.log(data);
})*/

/*dao.count('user', function(err,data){
	console.log(data)
})*/

// dao.quit()

/*getAllWhere("user", {iduser:1}, function(err, user){
	console.log(user);
})*/


/*getAll("user",function(err, users){
	console.log(users)
})*/




/*executarInstrucao(query, (function(data){
	console.log(data)
	console.log(data[0])
	console.log(Object.getPrototypeOf(data[0]))
}))*/

