"use strict";

export default class SQLStringBuilder {
	constructor(private string?: string, private argumentos?: any[]) { }

	// isString(data) {
	// 	return (typeof data);
	// }

	// isObj(data) {
	// 	return (typeof data);
	// }

	select(args: any) {
		this.string += "SELECT ?? ";
		this.argumentos.push(args);
		return this;
	}

	from(args: any, apelido: string = "") {
		if (apelido) {
			apelido += " ";
		}
		this.string += `FROM ?? ${apelido}`;
		this.argumentos.push(args);
		return this;
	}

	join(args: any, apelido: string) {
		apelido = apelido ? apelido + " " : "";
		this.string += `JOIN ?? ${apelido}`;
		this.argumentos.push(args);
		return this;
	}

	innerJoin(args: any, apelido: string) {
		apelido = apelido ? apelido + " " : "";
		this.string += `INNER JOIN ?? ${apelido}`;
		this.argumentos.push(args);
		return this;
	}

	leftJoin(args: any, apelido: string) {
		apelido = apelido ? apelido + " " : "";
		this.string += `LEFT JOIN ?? ${apelido}`;
		this.argumentos.push(args);
		return this;
	}

	rightJoin(args: any, apelido: string) {
		apelido = apelido ? apelido + " " : "";
		this.string += `RIGHT JOIN ?? ${apelido}`;
		this.argumentos.push(args);
		return this;
	}

	outerJoin(args: any, apelido: string) {
		apelido = apelido ? apelido + " " : "";
		this.string += `OUTER JOIN ?? ${apelido}`;
		this.argumentos.push(args);
		return this;
	}

	on(args: any) {
		this.string += `ON ${args} `;
		return this;
	}


	where(where: any) {
		this.string += "WHERE ? ";
		let first = true;
		const whereKeys = [];
		for (const key in where) {
			if (where.hasOwnProperty(key)) {
				if (!first) {
					this.string += " AND ?";
				}
				first = false;
				const whereKey: any = {};
				whereKey[key] = where[key];
				whereKeys.push(whereKey);
			}
		}
		
		this.argumentos.push(whereKeys);
		return this;
	}

	and(args: any) {
		this.string += "AND ??";
		this.argumentos.push(args);
		return this;
	}
}
