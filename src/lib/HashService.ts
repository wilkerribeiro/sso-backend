import * as bcrypt from "bcrypt";
export default class HashService {
	// Algorítimo Blowfish
	static saltRounds = 10;
	static hash(text: string, cb: (err: any, hash: string) => any) {
		bcrypt.hash(text, this.saltRounds, function (err, hash) {
			cb(err, hash);
		});
	}
	static compareToHash(text: string, hash: string, cb: (err: any, result: boolean) => any) {
		bcrypt.compare(text, hash, function (err, res) {
			cb(err, res);
		});
	}
}
