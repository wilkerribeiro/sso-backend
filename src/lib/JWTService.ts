import { sign, verify, JsonWebTokenError, VerifyCallback, SignOptions } from "jsonwebtoken";
import { Request } from "express";

/** @todo mudar algorítimo para RS256 */
const opts: SignOptions = {
	algorithm: "HS256",
	issuer: "localhost",
	expiresIn: 60 * 60,
	audience: "localhost"
};
const secretOrKey = "segredoDoJwtZxcka123zx";
const auth_scheme_lower = "bearer";
const auth_param = "authorization";

function parseAuthHeader(hdrValue: string | null) {
	if (typeof hdrValue !== "string") {
		return undefined;
	}
	const matches = hdrValue.match(/(\S+)\s+(\S+)/);
	return matches && { scheme: matches[1], value: matches[2] };
}

export default class JWTService {

	static gerarToken(payload: any, cb: (err: JsonWebTokenError, token: string) => any) {
		sign(payload, secretOrKey, opts, function (err: JsonWebTokenError, token) {
			cb(err, token);
		});
	}

	static verificar(token: string, cb: (err: JsonWebTokenError, decoded: any) => any) {
		verify(token, secretOrKey, function (err, decoded) {
			cb(err, decoded);
		});
	}
	static refresh(payload: any, refreshOptions: SignOptions, cb: (err: JsonWebTokenError, token: string) => any) {
		this.verificar(payload, (err, decoded) => {
			delete payload.iat;
			delete payload.exp;
			delete payload.nbf;
			delete payload.jti; // We are generating a new token, if you are using jwtid during signing, pass it in refreshOptions
			const jwtSignOptions = Object.assign({}, opts, { jwtid: refreshOptions.jwtid });
			sign(payload, secretOrKey, jwtSignOptions, function (err: JsonWebTokenError, token) {
				cb(err, token);
			});
		});
	}

	static getFormRequestHeader(request: Request) {
		let token;
		if (request.headers[auth_param]) {
			const auth_params = parseAuthHeader(request.headers[auth_param]);
			if (auth_params && auth_scheme_lower === auth_params.scheme.toLowerCase()) {
				token = auth_params.value;
			}
		}
		return token;
	}
}