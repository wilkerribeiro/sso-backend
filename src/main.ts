
import * as express from "express";
import * as tls from "tls";
import * as https from "https";
import * as http from "http";
import * as fs from "fs";
import initConfig from "./config/init-config";

// SSL
const options = {
	key: fs.readFileSync("./src/config/sslcert/key.pem"),
	cert: fs.readFileSync("./src/config/sslcert/cert.pem")
};
// const te = require('templateEngine')
// Create a service (the app object is just a callback).

const app = express();

initConfig(app);


app.use(function (req, res, next) {

	next();
});







// Criar o HTTP service.
http.createServer(app).listen(8091);
// criar o HTTPS service identico ao HTTP.
https.createServer(options, app).listen(443);
console.log("SERVER INICIADO:");
console.log("http: localhost:8091");
console.log("https: localhost:443");