export default class Permissao {
	id: number;
	perfil_id: number;
	codigo: string;
	descricao: string;
}