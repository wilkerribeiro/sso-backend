export default class Pessoa {
	id: number;
	primeiro_nome: string;
	segundo_nome: string;
	email: string;
	cpf: string;
}