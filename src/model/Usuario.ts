export default class User {
	id: number;
	login: string;
	hash: string;
}