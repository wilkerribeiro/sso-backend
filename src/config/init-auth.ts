import UsuarioDAO from "../DAO/UsuarioDAO";
import { Express, Request, Response, NextFunction } from "express";
import HashService from "../lib/HashService";
import JWTService from "../lib/JWTService";
import * as jwt from "jsonwebtoken";
import SistemaDAO from "../DAO/SistemaDAO";
import Sistema from "../model/Sistema";


export default function initAuth(app: Express) {
	app.get("/redirect", (req, res, next) => {
		res.redirect("http://localhost:4201/protegido");
		next();
	});


	// passport.use(jwtStrategy);
	// app.use(passport.initialize());
	app.get("/sso-auth/get-sistema/:sistema", (req, res) => {
		SistemaDAO.findOne({ codigo: req.params.sistema }, (err: any, sistema: Sistema) => {
			if (err) {
				res.status(500).send(err);
				return;
			}
			if (!sistema) {
				res.status(500).send(err);
				return;
			}
			res.send(sistema);
		});
	});

	app.post("/check-user", (req, res) => {
		const token = JWTService.getFormRequestHeader(req);
		if (!token) {
			res.status(401).send("Unauthorized");
		}
		JWTService.verificar(token, (err, jwt_payload) => {
			if (err) {
				res.status(400).send({
					error: "invalid_grant",
					error_description: err.message,
					error_type: err.name
				});
				return;
			}
			if (!jwt_payload) {
				res.status(401).send("Unauthorized");
				return;
			}
			console.log(jwt_payload)
			UsuarioDAO.findOne({ id: jwt_payload.id }, (err, user) => {
				if (err) {
					res.status(500).send(err);
					return;
				}
				if (user) {
					SistemaDAO.findOne({ codigo: req.body.sistemaKey }, (err: any, sistema: Sistema) => {
						if (err) {
							res.status(500).send(err);
							return;
						}
						if (!sistema) {
							res.status(500).send(err);
							return;
						}
						delete user.hash;
						res.send({ token, user, sistema });
					});
				} else {
					res.status(401).send("Unauthorized");
				}
			});
		});

	});

	app.post("/login", function (req, res) {
		if (!req.body.username || !req.body.password) {
			res.status(401).send({ message: "Preencha todos os campos" });
			return;
		}
		const sistemaKey = req.body.sistemaKey;
		const login = req.body.username;
		const password = req.body.password;
		UsuarioDAO.findOne({ login }, (err, user) => {
			if (err) {
				res.status(500).send(err);
				return;
			}
			HashService.compareToHash(password, user.hash, (err: any, result: boolean) => {
				if (err) {
					res.status(500).send(err);
					return;
				}
				if (!result) {
					res.sendStatus(401);
					return;
				}
				const payload = {
					id: user.id
				};
				JWTService.gerarToken(payload, (err, token) => {
					console.log(err);
					if (err) {
						res.status(500).send(err);
						return;
					}
					if (sistemaKey) {
						SistemaDAO.findOne({ codigo: req.body.sistemaKey }, (err: any, sistema: Sistema) => {
							if (err) {
								res.status(500).send(err);
								return;
							}
							if (!sistema) {
								res.status(500).send(err);
								return;
							}
							delete user.hash;
							res.json({ token, sistema, user });
						});
					} else {
						res.json({ token: token, user });
					}
				});
			});
		});

	});

	return () => (req: Request, res: Response, next: NextFunction) => {
		const token = JWTService.getFormRequestHeader(req);
		if (!token) {
			res.status(401).send("Unauthorized");
			return;
		}
		JWTService.verificar(token, (err, jwt_payload) => {
			if (!jwt_payload) {
				res.status(401).send("Unauthorized");
				return;
			}
			UsuarioDAO.findOne({ id: jwt_payload.id }, function (err, user) {
				if (err) {
					res.status(500).send(err);
				}
				if (user) {
					next();
				} else {
					res.status(401).send("Unauthorized");
				}
			});
		});

	};
}