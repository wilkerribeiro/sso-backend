import * as path from "path";
import * as bodyParser from "body-parser";

import { Response, Request, NextFunction, Express } from "express";
import * as helmet from "helmet";
// const cookieParser = require('cookie-parser')
import initAuth from "../config/init-auth";
import UsuarioDAO from "../DAO/UsuarioDAO";
import HashService from "../lib/HashService";

export default function initConfig(app: Express) {
	app.use(helmet());
	app.use(bodyParser.json());
	app.use(function (req: Request, res: Response, next: NextFunction) {

		// Website you wish to allow to connect
		const allowedOrigins = ["http://localhost:4200", "http://www.google.com.br", "https://localhost:4200", "http://localhost:4201", "https://localhost:4201"];
		const origin = req.headers.origin;
		if (allowedOrigins.indexOf(origin) > -1) {
			console.log("alowed");
			res.setHeader("Access-Control-Allow-Origin", origin);
		}

		res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");

		res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,Authorization");

		// res.setHeader("Access-Control-Allow-Credentials", "true");

		next();
	});

	// INICIALIZADNO O PASSPORT.JS
	const auth = initAuth(app);

	app.get("/test", function (req, res) {
		UsuarioDAO.findOne({ username: "wilker", email: "wilker7ribeiro@gmail.com" }, (err, data) => {
			res.send({ users: data, req: req.session });
		});
	});

	app.get("/getAll", function (req, res) {
		UsuarioDAO.getAll((err, data) => {
			res.send({ users: data, req: req.session });
		});
	});

	app.get("/get", auth(), function (req, res) {
		UsuarioDAO.getAll((err, data) => {
			res.send({ users: data, req: req.session });
		});
	});

	app.get("/findOne", function (req, res) {
		UsuarioDAO.findOne({ username: "wilker" }, (err, data) => {
			res.send({ users: data, req: req.session });
		});
	});

	app.get("/insert", function (req, res) {
		HashService.hash("ribeiro", (err, hash) => {
			const user = {
				login: "wilker",
				hash: hash
			};
			UsuarioDAO.insert(user, (err, response) => {
				res.send({ err, response });
			});
		});

	});

	app.get("/compare", function (req, res) {
		UsuarioDAO.getAllWhere({ username: "wilker" }, (err, dados) => {
			HashService.compareToHash("ribeiro", dados[0].hash, (err, response) => {
				res.status(200).send({ dados, response });
			});
		});
	});

	app.get("/auth", auth(),
		function (req, res) {
			res.send("req.user.profile");
		}
	);

	app.get("/delete", function (req, res) {
		UsuarioDAO.deleteWhere({ username: "wilker" }, (err, data) => {
			res.send({ data });
		});
	});

}