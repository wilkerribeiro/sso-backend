
import DAO from "../lib/database/DAO";

export default class SistemaDAO extends DAO {
	static tableName = "sistema";
}