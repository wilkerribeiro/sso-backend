import DAO from "../lib/database/DAO";

export default class PerfilDAO extends DAO {
	static tableName = "perfil";
}