import DAO from "../lib/database/DAO";

export default class PessoaDAO extends DAO {
	static tableName = "pessoa";
}